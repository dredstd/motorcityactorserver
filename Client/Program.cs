﻿using GrainInterfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Orleans;
using Orleans.Configuration;
using Orleans.Serialization.Configuration;

public class Program
{
	private static async Task<int> Main()
	{
		await RunClient();
		return 0;
	}

	private static async Task RunClient()
	{
		const int clients = 500;
		for (int i = 0; i < clients; i++)
		{
			try
			{
				IClusterClient client = await ConnectClient();
				Task.Run(() => TestWorld(client));
			}
			catch (Exception e)
			{
				Console.WriteLine($"\nException while trying to run client: {e.Message} \n\n{e.StackTrace}");
			}
		}
		Console.ReadKey();
		Console.WriteLine("\nEnd.");
	}

	private static async Task<IClusterClient> ConnectClient()
	{
		IHost host = new HostBuilder().UseOrleansClient(clientBuilder =>
		{
			clientBuilder.UseLocalhostClustering();
			clientBuilder.Configure<ClusterOptions>(options =>
			{
				options.ClusterId = "dev";
				options.ServiceId = "OrleansBasics";
			});
			clientBuilder.Configure<TypeManifestOptions>(options =>
			{
				options.AllowedTypes.Add("GrainInterfaces.IUserViewportGrain");
				options.AllowedTypes.Add("Root.Grains.UserViewportGrain");
			});
		}).Build();

		await host.StartAsync();
		IClusterClient client = host.Services.GetRequiredService<IClusterClient>();
		return client;
	}

	private static async Task TestWorld(IClusterClient client)
	{
		Random random = new();

		IVechicleGrain vechicleGrain = client.GetGrain<IVechicleGrain>(Guid.NewGuid()); //new vechicle
		const int maxEnum = 4;

		await vechicleGrain.Initilize((VechicleType)random.Next(maxEnum)); // set random type
		IUserGrain userGrain = client.GetGrain<IUserGrain>(Guid.NewGuid());
		await userGrain.AddVechicle(vechicleGrain);


		Guid worldGuid = Guid.Parse("a58c8021-5170-4691-8ca3-0357489e7e05"); // can be lot of worlds
		IWorldGrain worldGrain = client.GetGrain<IWorldGrain>(worldGuid);
		await worldGrain.Enter(userGrain);
		VechicleDynamic vechicleDynamic = await worldGrain.SpawnVechicle(userGrain, vechicleGrain);
		vechicleDynamic.Speed = Vector3.Forward;
		const float viewRadius = 500f;
		while (true)
		{
			const int timestamp = (int)(1000f / 60f);
			Thread.Sleep(timestamp);
			vechicleDynamic.Positon += vechicleDynamic.Speed;
			await worldGrain.UpdateTransform(vechicleGrain, vechicleDynamic.Positon, vechicleDynamic.Forward,
					vechicleDynamic.Forward);
			WorldBatch worldState = await worldGrain.GetWorldState(vechicleDynamic.Positon, viewRadius);
			//Console.WriteLine($"in view {worldState.Vechicles.Length} vechicles");
		}
	}
}