using Orleans;

namespace GrainInterfaces;

[GenerateSerializer]
public struct UserDynamic
{
	[Id(0)]
	public string Name;
	[Id(1)]
	public IVechicleGrain[] AvailableVechicles;

	public UserDynamic()
	{
		Name = "Unnamed";
		AvailableVechicles = Array.Empty<IVechicleGrain>();
	}

	public UserDynamic(string name, IVechicleGrain[] availableVechicles)
	{
		Name = name;
		AvailableVechicles = availableVechicles;
	}
}

[GenerateSerializer]
public class UserPersistance
{
	[Id(0)]
	public string Name;
	[Id(1)]
	public List<IVechicleGrain> AvailableVechicles;

	public UserPersistance()
	{
		Name = "Unnamed";
		AvailableVechicles = new List<IVechicleGrain>();
	}
}