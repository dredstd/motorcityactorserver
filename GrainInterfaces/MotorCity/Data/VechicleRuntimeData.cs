namespace GrainInterfaces;

public class VechicleRuntimeData
{
	public float Milleage;
	public readonly VechicleDynamic VechicleDynamic;

	public VechicleRuntimeData(VechicleDynamic vechicleDynamic)
	{
		VechicleDynamic = vechicleDynamic;
	}
}