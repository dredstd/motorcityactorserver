namespace GrainInterfaces;

public enum VechicleType : byte
{
	Undefined = 0,
	YamahaMt07 = 1,
	HondaHornet = 2,
	SuzukiHayabusa = 3
}