using Orleans;

namespace GrainInterfaces;

[GenerateSerializer]
public struct Vector3
{
	[Id(0)]
	public float X;
	[Id(1)]
	public float Y;
	[Id(2)]
	public float Z;

	public Vector3(float x, float y, float z)
	{
		X = x;
		Y = y;
		Z = z;
	}

	public override string ToString()
	{
		return $"X:{X} Y:{Y} Z:{Z}";
	}

	public static Vector3 Zero => new(0, 0, 0);
	public static Vector3 Forward => new(0, 0, 1);
	public static Vector3 operator -(Vector3 a, Vector3 b) => new(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
	public static Vector3 operator +(Vector3 a, Vector3 b) => new(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
	public float Magnitude => (float)Math.Sqrt(X * (double)X + Y * (double)Y + Z * (double)Z);

	public static Vector3 Random(Random random) =>
			new Vector3(random.NextSingle() * 1000f, 0, random.NextSingle() * 1000f);
}