using Orleans;

namespace GrainInterfaces;

[GenerateSerializer]
public class WorldBatch
{
	[Id(0)]
	public VechicleDynamic[] Vechicles;

	public WorldBatch()
	{
		Vechicles = Array.Empty<VechicleDynamic>();
	}

	public WorldBatch(VechicleDynamic[] vechicles)
	{
		Vechicles = vechicles;
	}
}