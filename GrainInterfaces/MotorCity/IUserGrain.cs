using Orleans;

namespace GrainInterfaces;

public interface IUserGrain : IGrainWithGuidKey
{
	Task<UserDynamic> GetData();
	Task SetName(string name);
	Task AddVechicle(IVechicleGrain vechicle);
	Task RemoveVechicle(IVechicleGrain vechicle);
}