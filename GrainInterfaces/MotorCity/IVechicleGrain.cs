using Orleans;

namespace GrainInterfaces;

public interface IVechicleGrain : IGrainWithGuidKey
{
	Task<VechicleType> GetData();
	Task Initilize(VechicleType vechicleType);
	Task AddMilleage(float meters);
}

[GenerateSerializer]
public class VechicleDynamic
{
	[Id(0)]
	public VechicleType VechicleType;
	[Id(1)]
	public Vector3 Positon;
	[Id(2)]
	public Vector3 Forward;
	[Id(3)]
	public Vector3 Speed;
	[Id(4)]
	public IUserGrain Owner;

	public VechicleDynamic(VechicleType vechicleType,
			Vector3 positon,
			Vector3 forward,
			Vector3 speed,
			IUserGrain user)
	{
		VechicleType = vechicleType;
		Positon = positon;
		Forward = forward;
		Speed = speed;
		Owner = user;
	}
}

[GenerateSerializer]
public class VechiclePersistance
{
	[Id(0)]
	public VechicleType VechicleType;
	[Id(1)]
	public float Mileage; //for example to meta progress or statistic
}