using Orleans;

namespace GrainInterfaces;

public interface IWorldGrain : IGrainWithGuidKey
{
	public Task Enter(IUserGrain user);
	public Task Exit(IUserGrain user);
	public Task<VechicleDynamic> SpawnVechicle(IUserGrain user, IVechicleGrain vechicle);

	public Task UpdateTransform(IVechicleGrain vechicle, Vector3 position, Vector3 forward, Vector3 speed);

	public Task<WorldBatch> GetWorldState(Vector3 position, float radius);
}