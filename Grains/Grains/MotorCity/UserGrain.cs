using GrainInterfaces;
using Orleans;
using Orleans.Runtime;

namespace Root.Grains;

public class UserGrain : Grain, IUserGrain
{
	private readonly IPersistentState<UserPersistance> _userState;

	public UserGrain(
			[PersistentState("UserPersistance", "UserStorage")]
			IPersistentState<UserPersistance> userState)
	{
		_userState = userState;
	}

	public Task<UserDynamic> GetData()
	{
		return Task.FromResult(new UserDynamic(_userState.State.Name,
				_userState.State.AvailableVechicles.ToArray()));
	}

	public async Task SetName(string name)
	{
		_userState.State.Name = name;
		await _userState.WriteStateAsync();
	}

	public async Task AddVechicle(IVechicleGrain vechicle)
	{
		if (_userState.State.AvailableVechicles.Contains(vechicle))
		{
			throw new Exception("Trying to add vechicle that already there");
		}
		_userState.State.AvailableVechicles.Add(vechicle);
		await _userState.WriteStateAsync();
	}

	public async Task RemoveVechicle(IVechicleGrain vechicle)
	{
		if (_userState.State.AvailableVechicles.Contains(vechicle) is false)
		{
			throw new Exception("Trying to remove vechicle that not there");
		}
		_userState.State.AvailableVechicles.Remove(vechicle);
		await _userState.WriteStateAsync();
	}
}