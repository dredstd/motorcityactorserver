using GrainInterfaces;
using Orleans;
using Orleans.Runtime;

namespace Root.Grains;

public class VechicleGrain : Grain, IVechicleGrain
{
	private readonly IPersistentState<VechiclePersistance> _persistentState;

	public VechicleGrain(
			[PersistentState("VechiclePersistance", "VechicleStorage")]
			IPersistentState<VechiclePersistance> persistentState)
	{
		_persistentState = persistentState;
	}

	public async Task Initilize(VechicleType vechicleType)
	{
		_persistentState.State.VechicleType = vechicleType;
		await _persistentState.WriteStateAsync();
	}

	public async Task AddMilleage(float meters)
	{
		_persistentState.State.Mileage += meters;
		await _persistentState.WriteStateAsync();
	}

	public Task<VechicleType> GetData()
	{
		return Task.FromResult(_persistentState.State.VechicleType);
	}
}