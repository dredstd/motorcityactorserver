using GrainInterfaces;
using Orleans;

namespace Root.Grains;

public class WorldGrain : Grain, IWorldGrain
{
	// TODO make more effective position based collection
	private readonly List<VechicleDynamic> _vechiclesDynamics = new();
	private readonly Dictionary<IVechicleGrain, VechicleRuntimeData> _vechicleRuntimeByVechicle = new();
	private readonly Dictionary<IUserGrain, IVechicleGrain> _vechicleByUser = new();
	private readonly Random _random = new();
	private readonly List<VechicleDynamic> _batchResult = new();

	public Task Enter(IUserGrain user)
	{
		if (_vechiclesDynamics.Count > 1000)
		{
			throw new Exception("Too much players");
		}
		return Task.CompletedTask;
	}

	public async Task Exit(IUserGrain user)
	{
		IVechicleGrain vechicle = _vechicleByUser[user];
		VechicleRuntimeData vechicleRuntimeData = _vechicleRuntimeByVechicle[vechicle];
		await vechicle.AddMilleage(vechicleRuntimeData.Milleage);
		_vechicleByUser.Remove(user);
		_vechicleRuntimeByVechicle.Remove(vechicle);
		_vechiclesDynamics.Remove(vechicleRuntimeData.VechicleDynamic);
	}

	public async Task<VechicleDynamic> SpawnVechicle(IUserGrain user, IVechicleGrain vechicle)
	{
		Vector3 position = Vector3.Random(_random);
		VechicleType vechicleType = await vechicle.GetData();
		VechicleDynamic vechicleDynamic = new(vechicleType, position, Vector3.Forward, Vector3.Zero, user);
		VechicleRuntimeData runtimeData = new(vechicleDynamic);
		_vechicleRuntimeByVechicle.Add(vechicle, runtimeData);
		_vechicleByUser.Add(user, vechicle);
		_vechiclesDynamics.Add(vechicleDynamic);
		return vechicleDynamic;
	}

	public Task UpdateTransform(IVechicleGrain vechicle, Vector3 position, Vector3 forward, Vector3 speed)
	{
		VechicleRuntimeData vechicleRuntimeData = _vechicleRuntimeByVechicle[vechicle];
		VechicleDynamic vechicleDynamic = vechicleRuntimeData.VechicleDynamic;
		vechicleRuntimeData.Milleage += (position - vechicleDynamic.Positon).Magnitude;
		vechicleDynamic.Forward = forward;
		vechicleDynamic.Speed = speed;
		vechicleDynamic.Positon = position;
		return Task.CompletedTask;
	}

	public Task<WorldBatch> GetWorldState(Vector3 position, float radius)
	{
		_batchResult.Clear();

		foreach (VechicleDynamic vechicleDynamic in _vechiclesDynamics)
		{
			if ((vechicleDynamic.Positon - position).Magnitude < radius)
			{
				_batchResult.Add(vechicleDynamic);
			}
		}
		return Task.FromResult(new WorldBatch(_batchResult.ToArray()));
	}
}