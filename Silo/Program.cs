﻿using Microsoft.Extensions.Hosting;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;

public static class Program
{
	private static int Main()
	{
		return RunMainAsync().Result;
	}

	private static async Task<int> RunMainAsync()
	{
		try
		{
			var host = await StartSilo();
			Console.WriteLine("\n\n Press Enter to terminate...\n\n");
			Console.ReadLine();

			await host.StopAsync();

			return 0;
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex);
			return 1;
		}
	}

	private static async Task<IHost> StartSilo()
	{
		// define the cluster configuration

		IHost host = Host.CreateDefaultBuilder().UseOrleans(builder =>
		{
			builder.UseLocalhostClustering();
			builder.Configure<ClusterOptions>(options =>
			{
				options.ClusterId = "dev";
				options.ServiceId = "OrleansBasics";
			});
			//TODO add db storage
			builder.AddMemoryGrainStorage("VechicleStorage");
			builder.AddMemoryGrainStorage("UserStorage");
			builder.UseDashboard();
		}).Build();

		await host.StartAsync();
		return host;
	}
}